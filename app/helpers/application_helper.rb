# frozen_string_literal: true

module ApplicationHelper
  def full_title(page_title)
    base_title = 'BIGBAG Store'
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def shop_active(active)
    'active' if active.blank?
  end
end
