# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe 'GET products#show' do
    context 'productが存在する場合' do
      let(:product) { create :product }

      before do
        get "/potepan/products/#{product.id}"
      end

      it 'Webリクエストが成功する' do
        expect(response).to be_success
        expect(response.status).to eq 200
      end

      it 'showテンプレートが表示される' do
        expect(response).to render_template(:show)
      end

      it '@productが取得できている' do
        expect(assigns(:product)).to eq product
      end

      it '商品名が含まれる' do
        expect(response.body).to include(product.name)
      end

      it '商品詳細が含まれる' do
        expect(response.body).to include(product.description)
      end

      it '商品価格が含まれる' do
        expect(response.body).to include(product.display_price.to_s)
      end
    end

    context 'productが存在しない場合' do
      subject { -> { get potepan_product_url 1 } }
      it { is_expected.to raise_error ActiveRecord::RecordNotFound }
    end
  end
end
